#include <algorithm>
#include <cassert>
#include <cstdint>
#include <cstdlib>
#include <chrono>
#include <cmath>
#include <iostream>
#include <memory>
#include <vector>

static const size_t align = 4096;

using clock_type = std::chrono::high_resolution_clock;

extern "C"
{
   extern void dgemv_(char*, int*, int*, double*, double*, int*, double*, int*, double*, double*, int*);
}

void dgemv(char TRANS, int M, int N, double ALPHA, double* A, int LDA, double* X, int INCX, double BETA, double* Y, int INCY)
{
   dgemv_(&TRANS, &M, &N, &ALPHA, A, &LDA, X, &INCX, &BETA, Y, &INCY);
}

void MV_dense_column_wise_BLAS(double * A, double * x, double * y, int n)
{
   dgemv('N', n, n, 1.0, A, n, x, 1, 1.0, y, 1);
}

void MV_dense_column_wise_custom(const double * __restrict A, const double * __restrict x, double * __restrict y, const uint64_t n)
{
   const double * __restrict A_ = std::assume_aligned<align>(A);
   const double * __restrict x_ = std::assume_aligned<align>(x);
   double * __restrict y_ = std::assume_aligned<align>(y);

   uint64_t k = 0;
   for (uint64_t j = 0; j < n; j++)
      for (uint64_t i = 0; i < n; i++)
         y[i] += x[j] * A[k++];
}

double norm2(const double * y, uint64_t n)
{
   double res = 0.0;
   for (uint64_t i = 0; i < n; i++)
      res += y[i] * y[i];
   return std::sqrt(res);
}

int main(int argc, char* argv[])
{
   if (argc < 2)
   {
      std::cerr << "Usage: ./program <size>" << std::endl;
      std::exit(1);
   }

   const long n = std::atol(argv[1]);
   std::cout << "Matrix size: " << n << std::endl;

   double * A = (double*)std::aligned_alloc(align, sizeof(double) * n * n);
   double * x = (double*)std::aligned_alloc(align, sizeof(double) * n);
   double * y = (double*)std::aligned_alloc(align, sizeof(double) * n);

   std::fill(A, A + n * n, 1.0);
   std::fill(x, x + n, 1.0);

   for (int iter = 0; iter < 10; iter++)
   {
      std::fill(y, y + n, 0.0);

      auto start = clock_type::now();

//    MV_dense_column_wise_custom(A, x, y, n);
      MV_dense_column_wise_BLAS(A, x, y, n);

      auto end = clock_type::now();
      std::chrono::duration<double> duration = end - start;

      uint64_t flop = n * n * 2;
      uint64_t flop_s = (double)flop / duration.count();

      std::cout << "Iteration " << iter
         << ", expected = " << std::sqrt((double)n * n * n)
         << ", calculated = " << norm2(y, n)
         << ", elapsed time = " << duration.count() << " [s]"
         << ", Gflop/s = " << flop_s / 1000.0 / 1000.0 / 1000.0 
         << std::endl;
   }
}
